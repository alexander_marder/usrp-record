#! /usr/bin/env python3
import os.path
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
import subprocess as sp
from datetime import datetime

from arfcn_calc import earfcn2freq


def main():
    parser = ArgumentParser(formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument('-e', '--earfcn', type=int)
    parser.add_argument('-d', '--duration', type=int, default=3600)
    parser.add_argument('--args', default='master_clock_rate=23.04e6')
    parser.add_argument('--gain', type=int, default=20)
    parser.add_argument('--ant', default='TX/RX')
    parser.add_argument('--rate', default='23.04e6')
    args = parser.parse_args()

    freq = earfcn2freq(args.earfcn)

    dt = datetime.now()
    filename = os.path.join('/capture', f'{freq}_{dt.strftime("%Y%m%dT%H%M")}.dat')

    cmd = f'/usr/lib/uhd/examples/rx_samples_to_file --file {filename} --freq {freq}e6 --rate {args.rate} --gain {args.gain} --duration {args.duration}  --ant {args.ant} --args {args.args}'
    print(cmd)
    sp.run(cmd, shell=True)

if __name__ == '__main__':
    main()
