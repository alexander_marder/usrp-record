#!/usr/bin/env bash

Help() {
    echo "Usage: $0 [Options...]" >&2
    echo "  -i, --image <Image name>        Name of the Docker image (e.g. record)"
    echo "  -o, --out  <Output folder>      Output folder for the capture"
    echo "  -e, --earfcn <EARFCN>           EARFCN value"
    echo "  -d, --duration <Duration>       Capture duration"
    echo "  -h, --help                      Show help menu"
    echo "Examples:"
    echo "  $0 --image record --earfcn 700 --duration 3600 --out test"
    exit 1
}

TIMEOUT=0
IMAGE=record
while [[ $# -gt 0 ]]; do
  case $1 in
    -i|--image)
      IMAGE="$2"
      shift # past argument
      shift # past value
      ;;
    -e|--earfcn)
      EARFCN=$2
      shift # past argument
      shift # past value
      ;;
    -o|--out)
      OUTPUT=$2
      shift # past argument
      shift # past value
      ;;
    -d|--duration)
      TIMEOUT=$2
      shift
      shift
      ;;
    -h|--help)
      Help
      ;;
    *)
      echo "Unknown option $1"
      exit 1
      ;;
  esac
done

CAPTURE=$(realpath $OUTPUT)

docker run -ti --privileged -v /dev:/dev -v /proc:/proc --rm -v $CAPTURE:/capture $IMAGE python3 record.py -d $TIMEOUT -e $EARFCN