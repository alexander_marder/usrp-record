FROM ubuntu:jammy
ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update
#RUN apt-get upgrade -y
RUN apt-get -y install nano software-properties-common python3 libuhd-dev uhd-host
#RUN apt-get install -y build-essential software-properties-common git cmake libfftw3-dev libmbedtls-dev libboost-program-options-dev libconfig++-dev libsctp-dev python3 python3-pip python3-setuptools nano

#RUN add-apt-repository ppa:ettusresearch/uhd
#RUN apt-get update
#RUN apt-get install -y libuhd-dev uhd-host
RUN uhd_images_downloader

COPY tools/* .
